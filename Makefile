# NAME: Omar Tleimat
# ID: 404844706
# EMAIL: otleimat@ucla.edu
	
all_files = lab3b-404844706.tar.gz lab3b.py README Makefile

.SILENT:

default:
	echo 'python lab3b.py $$1' > lab3b
	chmod +x lab3b

clean: 
	rm -f lab3b-404844706.tar.gz lab3b

dist:
	tar -czvf $(all_files)

 