#!/usr/bin/python
# NAME: Omar Tleimat
# ID: 404844706
# EMAIL: otleimat@ucla.edu
import sys

class System(object):

	def init(self):
		if len(sys.argv) != 2:
			print("Usage: ./lab3b CSV_FILE")
			sys.exit(1)

		with open(sys.argv[1]) as f:
			self.file = f.read().splitlines()

		# class variable to keep track of all blocks referenced
		# to test for unreferenced blocks at the end of the program
		self.blocks_ref = []
		# self.super_block ex. content
		# ['SUPERBLOCK', '64', '24', '1024', '128', '8192', '24', '11']
		self.super_block = self.file[0].split(",")
		self.num_blocks, self.num_inodes, self.reserved = self.get_sb_values()
		self.ifree = self.get_free_list('IFREE')
		self.bfree = self.get_free_list('BFREE')
		self.inodes = self.get_list('INODE')
		self.dirs = self.get_list('DIRENT')
		self.indirs = self.get_list('INDIRECT')
		self.inode_links = {}
		self.assign_link_counts()
		self.process_inodes()
		self.check_unalloc_inodes()
		#self.check_ref_blocks()
		# prints for testing purposes
		#print(self.file)
		#print(self.super_block)
		#print(self.bfree)
		#print(self.ifree)
		#print(self.inodes)
		#print(self.indirs)
		#print(self.blocks_ref)

	def assign_link_counts(self):
		for inode in self.inodes:
			self.inode_links[str(inode[1])] = 0
		for d in self.dirs:
			self.inode_links[d[3]] += 1

	def get_sb_values(self):
		return int(self.super_block[1]), int(self.super_block[2]), int(self.super_block[7])

	def get_free_list(self, type):
		'''
		@param -type Type of element this should return e.g. 'IFREE'
		Returns an integer array of free elements of specified type
		Example return: 
		[18, 19, 20, 21, 22, 23, 24]
		'''
		return [int(line.split(",")[1]) for line in self.file if type in line]

	def get_list(self, type):
		'''
		@param type: type of element this should return e.g. 'INODE', 'DIRENT', 'INDIRECT'
		Returns an array of full lines containing matches with type parameter
		Example return:
		[['INDIRECT', '16', '1', '12', '34', '35'], ['INDIRECT', '16', '1', '13', '34', '36'],
		 ['INDIRECT', '17', '1', '14', '61', '37']]
		'''
		return [line.split(",") for line in self.file if type in line]

	def process_inodes(self):
		# traverse all INODE lines

		if self.no_duplicates():
			for inode in self.inodes:
				inode_num = int(inode[1])
				num_blocks = int(inode[11])
				links = inode[6]
				self.check_links(links, inode_num)
				if self.item_in_free_list(inode_num, self.ifree):
					print_inv_alloc_inode(inode_num)
				# blocks contains the last 15 block members of the inode
				blocks = inode[12:]
				for index, block in enumerate(blocks):
					block = int(block)
					# if the number of blocks in the inode is zero,
					# we should check for invalid blocks or reserved blocks 
					if num_blocks == 0:
						if block > 0:
							if not self.is_valid_block(block):
								report_invalid_block(index, block, inode_num)
							elif not self.is_reserved_inode(inode_num):
								report_reserved_block(index, block, inode_num)
					else:
						# record referenced blocks
						if block > 0:
							self.blocks_ref.append(block)
						# check to see if an allocated block is in the free list
						if self.item_in_free_list(block, self.bfree):
							print_inv_alloc_block(block)
						
		

	# Checks for unreferenced blocks
	# this isnt fully functional yet since we havent 
	# started implemented check of indirect blocks
	def check_ref_blocks(self):
		# Go through block 1...num_blocks
		# if a block wasnt free and also not referenced, print unref msg.
		for block_num in xrange(1, self.num_blocks):
			if block_num not in self.bfree and block_num not in self.blocks_ref:
				print_unref_block(block_num)          

	def check_links(self, links, no):
		if self.inode_links[str(no)] != int(links):
			print("INODE %d HAS %d LINKS BUT LINKCOUNT IS %s" %(no, self.inode_links[str(no)], links))

	def no_duplicates(self):
		'''
		!!!This was pretty hard to implement. Say thank you!!!
		'''
		block_array = [inode[12:] for inode in self.inodes]
		ino_block = {}
		none = True
		for i, block in enumerate(block_array):
			block_array[i] = [int(b) for b in block if int(b) > 0]
			for b in block_array[i]:
				ino_block[str(b)] = []

		for i, block in enumerate(block_array):
			for b in block_array[i]:
				ino_block[str(b)].append(self.inodes[i][1])
		
		for key in ino_block:
			if len(ino_block[key]) > 1:
				self.report_duplicate(key, ino_block)
				none = False
		return none

	def check_unalloc_inodes(self):
		inode_nums = [int(inode[1]) for inode in self.inodes]
		for x in xrange(0, self.num_inodes + 1):
			if x == 2 or x >= self.reserved:
				if x not in inode_nums and x not in self.ifree:
					print("UNALLOCATED INODE %d NOT ON FREELIST" % (x)) 

	def report_duplicate(self, key, block_dict):
		inode_nums = block_dict[key]
		for inode in self.inodes:
			inode_num = inode[1]
			if inode_num in inode_nums:
				print_dup(inode[12:].index(key), inode_num, key)




	''' 
	CLASS HELPER FUNCTIONS
	'''

	def item_in_free_list(self, num, list):
		for item in list:
			if item == num:
				return True
		return False

	def is_valid_block(self, block_num):
		return True if block_num <= self.num_blocks else False
	def is_reserved_inode(self, inode_num):
		return True if (inode_num > 2 and inode_num < self.reserved) else False

'''
OUTSIDE OF CLASS HELPER/UTILITY FUNCTIONS
'''

def print_dup(index, inode_num, block):
	if index < 12:
		msg = "DUPLICATE"
	elif index == 12:
		msg = "DUPLICATE INDIRECT"
	elif index == 13:
		msg = "DUPLICATE DOUBLE INDIRECT"
	elif index == 14:
		msg = "DUPLICATE TRIPLE INDIRECT"
	print_block_msg(msg, int(block), int(inode_num), get_offset(index))

def report_reserved_block(index, block_num, inode_num):
	if index < 12:
		msg = "RESERVED"
	elif index == 12:
		msg = "RESERVED INDIRECT"
	elif index == 13:
		msg = "RESERVED DOUBLE INDIRECT"
	elif index == 14:
		msg = "RESERVED TRIPLE INDIRECT"
	print_block_msg(msg, block_num, inode_num, get_offset(index))


def report_invalid_block(index, block_num, inode_num):
	if index < 12:
		msg = "INVALID"
	elif index == 12:
		msg = "INVALID INDIRECT"
	elif index == 13:
		msg = "INVALID DOUBLE INDIRECT"
	elif index == 14:
		msg = "INVALID TRIPLE INDIRECT"
	print_block_msg(msg, block_num, inode_num, get_offset(index))

def get_offset(index):
	if index <= 12:
		offset = index
	elif index == 13:
		offset = 268
	elif index == 14:
		offset = 65804
	return offset

def print_block_msg(msg, block_num, inode_num, offset):
		print("%s BLOCK %d IN INODE %d AT OFFSET %d" % (msg, block_num, inode_num, offset))

def print_inv_alloc_block(block):
	print("ALLOCATED BLOCK %d ON FREELIST" % (block))

def print_inv_alloc_inode(inode):
	print("ALLOCATED INODE %d ON FREELIST" % (inode))

def print_unref_block(block):
	print("UNREFERENCED BLOCK %d" % (block))

if __name__ == "__main__":
    System().init()
